# -*- coding: utf-8 -*-

import json
import logging
from time import sleep
import daemon
from daemon import pidfile
from protonmail.client import Client
from protonmail.responses import ConversationsResponse, ConversationResponse
import sleekxmpp
import ssl
import os.path


__CONFIG_FILENAME = 'config.json'
__CONFIG = {}
__EMAILS = []
__logger = None
__history = []


class SendMsgBot(sleekxmpp.ClientXMPP):

    """
    A basic SleekXMPP bot that will log in, send a message,
    and then log out.
    """

    def __init__(self, jid, password, recipient, messages):
        sleekxmpp.ClientXMPP.__init__(self, jid, password)

        # The message we wish to send, and the JID that
        # will receive it.
        self.recipient = recipient
        self.msgs = messages

        # The session_start event will be triggered when
        # the bot establishes its connection with the server
        # and the XML streams are ready for use. We want to
        # listen for this event so that we we can initialize
        # our roster.
        self.add_event_handler("session_start", self.start)

    def start(self, event):
        """
        Process the session_start event.

        Typical actions for the session_start event are
        requesting the roster and broadcasting an initial
        presence stanza.

        Arguments:
            event -- An empty dictionary. The session_start
                     event does not provide any additional
                     data.
        """
        self.send_presence()
        self.get_roster()

        for msg in self.msgs:
            self.send_message(mto=self.recipient,
                              mbody=msg,
                              mtype='chat')

        # Using wait=True ensures that the send queue will be
        # emptied before ending the session.
        self.disconnect(wait=True)


def set_file_logging():
    global __logger

    __logger = logging.getLogger('proton2jabber')
    __logger.setLevel(logging.INFO)

    fh = logging.FileHandler(__CONFIG["logfile"])
    fh.setLevel(logging.INFO)

    formatstr = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    formatter = logging.Formatter(formatstr)
    fh.setFormatter(formatter)

    __logger.addHandler(fh)


def send_to_jabber(messages):
    xmpp = SendMsgBot(__CONFIG["jabber_login"], __CONFIG["jabber_pwd"], __CONFIG["jabber_login"], messages)
    xmpp.ssl_version = ssl.PROTOCOL_SSLv23

    if xmpp.connect():
        xmpp.process(block=True)
        __logger.info("Done")
    else:
        __logger.error("Unable to connect.")


def check_mail(login, pwd):
    global __logger
    result = []

    client = Client(Username=login, blocking=True)  
    client.api.login(pwd)

    conv_response = client.api.conversations(response=ConversationsResponse)
    for conversation in conv_response.Conversations:
        if conversation.ID in __history:
            continue

        chain = client.api.conversations(conversation.ID, response=ConversationResponse)
        for message in chain.Messages:
            if (message.Unread):
                message_data = client.read_message(message)
                parsed = {
                    "ID": message.ID,
                    "From": "{}({})".format(message.Sender.Name, message.Sender.Address),
                    "msg": message.DecryptedBody
                }
                __history.append(parsed)
                result.append(json.dumps(parsed))

        __history.append(conversation.ID)
        # a = client.api.conversations.read(method='PUT', body={'IDs': [conversation.ID]}) 


    __logger.info("{} messages parsed".format(len(result)))
    client.api.logout()
    return result


def do_job():
    global __history
    HIST_FILENAME = "hist.json"

    with open(__CONFIG["email_file"], 'r') as f:
        __EMAILS.extend(f.readlines())

    while True:
        if os.path.isfile(HIST_FILENAME):
            with open(HIST_FILENAME, 'r') as f:
                __history = json.loads(''.join(f.readlines()))

        for email in __EMAILS:
            try:
                e_data = email.split(':')
                send_to_jabber(check_mail(e_data[0], e_data[1]))
                # check_mail(e_data[0], e_data[1])
            except Exception:
                __logger.error("Something went wrong")
            finally:
                sleep(__CONFIG["update_period"])

        with open(HIST_FILENAME, 'w') as f:
            f.write(json.dumps(__history))


def main():
    global __CONFIG

    with open(__CONFIG_FILENAME, 'r') as f:
        __CONFIG = json.loads('\n'.join(f.readlines()))

    print("Config loaded")
    set_file_logging()

    # ### XXX pidfile is a context
    # with daemon.DaemonContext(
    #     # working_directory='/var/lib/proton2jabber',
    #     umask=0o002,
    #     pidfile=pidfile.TimeoutPIDLockFile(__CONFIG["pid_file"])
    # ) as context:
    #     do_job()
    do_job()


if __name__ == '__main__':
    main()